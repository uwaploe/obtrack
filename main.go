// Obtrack tracks the residence time of a file in the OUTBOX of
// the DOT Buoy controller.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"
)

var usage = `Usage: obtrack [options] statefile outfile

Utility to track the residence time of files in the OUTBOX. This program
is designed to be run periodically by Cron or Systemd.
`

var (
	obDir = flag.String("outbox", "/data/OUTBOX", "Outbox directory name")
)

type dataRecord struct {
	Name  string
	Delay time.Duration
}

func loadState(fname string) (map[string]time.Time, error) {
	var err error
	m := make(map[string]time.Time)
	if f, err := os.Open(fname); err == nil {
		defer f.Close()
		dec := json.NewDecoder(f)
		err = dec.Decode(&m)
	}
	return m, err
}

func saveState(fname string, state map[string]time.Time) error {
	f, err := os.Create(fname)
	if err != nil {
		return err
	}
	defer f.Close()
	enc := json.NewEncoder(f)
	return enc.Encode(state)
}

func readOutbox(dirname string) (map[string]time.Time, error) {
	m := make(map[string]time.Time)
	files, err := ioutil.ReadDir(dirname)
	if err != nil {
		return nil, err
	}
	for _, f := range files {
		m[f.Name()] = f.ModTime().UTC()
	}
	return m, nil
}

// Return the elements from A that aren't in B
func setDiff(a, b map[string]time.Time) map[string]time.Time {
	d := make(map[string]time.Time)
	for k, v := range a {
		_, found := b[k]
		if !found {
			d[k] = v
		}
	}
	return d
}

func checkOutbox(dirname string, state map[string]time.Time) ([]dataRecord, error) {
	t := time.Now().UTC()
	recs := make([]dataRecord, 0)
	m, err := readOutbox(dirname)
	if err != nil {
		return nil, err
	}

	// Remove files that have been transferred
	for k, v := range setDiff(state, m) {
		recs = append(recs, dataRecord{Name: k, Delay: t.Sub(v)})
		delete(state, k)
	}

	// Add new files
	for k, v := range setDiff(m, state) {
		state[k] = v
	}

	return recs, nil
}

func addRecords(fname string, recs []dataRecord) error {
	f, err := os.OpenFile(fname, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	for _, rec := range recs {
		fmt.Fprintf(f, "%s %d\n", rec.Name, int64(rec.Delay/time.Second))
	}

	return nil
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}
	flag.Parse()

	args := flag.Args()

	if len(args) < 2 {
		flag.Usage()
		os.Exit(1)
	}

	state, err := loadState(args[0])
	if err != nil {
		log.Fatalf("loadState: %v", err)
	}

	recs, err := checkOutbox(*obDir, state)
	if err != nil {
		log.Fatalf("readOutbox: %v", err)
	}

	err = saveState(args[0], state)
	if err != nil {
		log.Fatalf("saveState: %v", err)
	}

	if len(recs) > 0 {
		err = addRecords(args[1], recs)
		if err != nil {
			log.Fatalf("addRecords: %v", err)
		}
	}
}
