# OUTBOX File Tracker

Obtrack is a simple application to track how long files stay in the OUTBOX on the DOT Buoy Controller. This provides a measure of the Iridium file transfer performance.
